import { Action } from '@ngrx/store';
import { User } from '../model/user.model';

export enum AuthActionTypes {
  LoginAction = '[Auth] Login action',
  LogoutAction = '[Auth] Logout action'
}

export class LoginAction implements Action {
  readonly type = AuthActionTypes.LoginAction;
  constructor(public payload: { user: User }) { }
}


export type AuthActions = LoginAction;
